class Book

attr_accessor :title

def title=(old_title)
  lower = ["in", "the", "of", "and", "an", "a"]
  newtitle = old_title.split.map{|x|lower.include?(x) ? x.downcase : x.capitalize}
  newtitle[0].capitalize!
  @title = newtitle.join(" ")

end

end
