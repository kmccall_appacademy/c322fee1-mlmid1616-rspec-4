class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_format(time)
    if time.to_s.length > 1
      time.to_s
    else
      "0"+time.to_s
    end
  end

  def time_string
    standard_time = [0,0,0]
    standard_time[0] = time_format(seconds / 3600)
    standard_time[1] = time_format((seconds % 3600)  / 60)
    standard_time[2] = time_format(seconds % 60)
    @time_string = standard_time.join(":")
  end
    
end






















# class Timer
#
# attr_accessor :seconds
#
# def initialize(seconds=0)
#   @seconds = seconds
# end
#
# def time_format(time)
#   if time.to_s.length > 1
#     time.to_s
#   else
#     "0" + time.to_s
#   end
# end
#
# def time_string
#   standard_time =[0,0,0]
#   standard_time[0] = time_format(seconds/3600)
#   standard_time[1] = time_format((seconds % 3600) / 60)
#   standard_time[2] = time_format(seconds % 60)
#   @time_string = standard_time.join(":")
# end
#
#
# end


# class Timer
#   attr_accessor :seconds
#
#   def initialize(seconds = 0)
#     @seconds = seconds
#   end
#
#   def padded(n)
#     return "0#{n}" if n < 10
#     "#{n}"
#   end
#
#   def time_string
#     hours = seconds / 3600
#     minutes = (seconds % 3600) / 60
#     timer_seconds = seconds % 60
#     "#{padded(hours)}:#{padded(minutes)}:#{padded(timer_seconds)}"
#   end
# end
