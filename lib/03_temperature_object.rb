# class Temperature
#
#   attr_accessor :temperature, :scale
#
#   def initialize(opts = {:f => 0, :c => 0})
#     if opts[:f]
#       @temperature = opts[:f]
#       @scale = :fahrenheit
#     else
#       @temperature = opts[:c]
#       @scale = :celsius
#     end
#   end
#
#   def in_fahrenheit
#     if opts[:f]
#       @in_fahrenheit = opts[:f]
#     else
#       @in_fahrenheit = opts[:c] * (9/5.0)
#     end
#   end
#
# end


# describe Temperature do
#   describe "can be constructed with an options hash" do
#     describe "in degrees fahrenheit" do
#       it "at 50 degrees" do
#         expect(Temperature.new(:f => 50).in_fahrenheit).to eq(50)
#       end
#
#       describe "and correctly convert to celsius" do
#         it "at freezing" do
#           expect(Temperature.new(:f => 32).in_celsius).to eq(0)
#         end
#
#         it "at boiling" do
#           expect(Temperature.new(:f => 212).in_celsius).to eq(100)
#         end
#
#         it "at body temperature" do
#           expect(Temperature.new(:f => 98.6).in_celsius).to eq(37)
#         end
#
#         it "at an arbitrary temperature" do
#           expect(Temperature.new(:f => 68).in_celsius).to eq(20)
#         end
#       end
#     end





















class Temperature
    attr_accessor :temperature, :scale

    def initialize(opts = {:f => 0, :c => 0})
        if opts[:f]
            @temperature = opts[:f]
            @scale = :fahrenheit
        elsif opts[:c]
            @temperature = opts[:c]
            @scale = :celsius
        end
    end

    def in_celsius
        return @temperature if @scale == :celsius
        (5.00/9.00) * (@temperature - 32)
    end

    def in_fahrenheit
        return @temperature if @scale == :fahrenheit
        self.class.ctof(@temperature)
    end

    def self.from_celsius(degrees)
        Temperature.new(:c => degrees)
    end

    def self.from_fahrenheit(degrees)
        self.new(:f => degrees)
    end

    def self.ftoc(temperature)
        (5.00/9.00)*((temperature) - 32)
    end

    def self.ctof(temperature)
        (9.0/5.0)*(temperature) + 32
    end


end

class Celsius < Temperature
    def initialize(degrees)
      @temperature = degrees
      @scale = :celsius
    end
end

class Fahrenheit < Temperature
    def initialize(degrees)
      @temperature = degrees
      @scale = :fahrenheit
    end
end
